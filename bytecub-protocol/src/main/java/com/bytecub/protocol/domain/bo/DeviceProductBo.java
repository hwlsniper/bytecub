package com.bytecub.protocol.domain.bo;

import com.bytecub.protocol.base.IBaseProtocol;
import lombok.Data;

/**
 *  * ByteCub.cn.
 *  * Copyright (c) 2020-2021 All Rights Reserved.
 *  * 
 *  * @author bytecub@163.com  songbin
 *  * @Date 2021/4/2  Exp $$
 *  
 */
@Data
public class DeviceProductBo {
    IBaseProtocol baseProtocol;
    String productCode;
    String deviceCode;
}
