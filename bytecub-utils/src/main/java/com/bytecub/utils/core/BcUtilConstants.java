package com.bytecub.utils.core;

/**
 * com.bytecub.utils.core
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/8
 */
public class BcUtilConstants {
    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile/";
}
