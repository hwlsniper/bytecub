package com.bytecub.udp.service.parser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 *  * ByteCub.cn.
 *  * Copyright (c) 2020-2021 All Rights Reserved.
 *  * UDP授权校验
 *  * @author bytecub@163.com  songbin
 *  * @Date 2021/4/2  Exp $$
 *  
 */
@Service
@Slf4j
public class UdpAuthParser {

    /**
     * @param content
     * @return true or false
     * */
    public Boolean verify(byte[] content){
        return true;
    }
}
