package com.bytecub.manager.controller.firmware;

import java.util.Date;
import java.util.List;

import com.bytecub.manager.mq.DelayUpgradeProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.DataResult;
import com.bytecub.common.domain.dto.PageReqDto;
import com.bytecub.common.domain.dto.PageResDto;
import com.bytecub.common.domain.dto.request.upgrade.UpgradeDeviceQueryReqDto;
import com.bytecub.common.domain.dto.request.upgrade.UpgradeReqDto;
import com.bytecub.common.domain.dto.response.device.DevicePageResDto;
import com.bytecub.common.enums.BCErrorEnum;
import com.bytecub.common.exception.BCGException;
import com.bytecub.manager.service.IUpgradeService;
import com.bytecub.mdm.dao.dal.FirmwareTaskDetailPoMapper;
import com.bytecub.mdm.dao.dal.FirmwareTaskPoMapper;
import com.bytecub.mdm.dao.po.FirmwareTaskDetailPo;
import com.bytecub.mdm.dao.po.FirmwareTaskPo;
import com.bytecub.mdm.service.IDeviceService;
import com.bytecub.mdm.service.IFirmwareService;
import com.github.pagehelper.PageHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 *  * ByteCub.cn.  * Copyright (c) 2020-2020 All Rights Reserved.  *   * @author bytecub@163.com songbin
 *  * @version Id: DeviceController.java, v 0.1 2020-12-22  Exp $$  
 */
@Slf4j
@RestController
@RequestMapping(BCConstants.URL_PREFIX.MGR + "firmware/task")
@Api(tags = "升级任务管理")
public class FirmwareTaskController {

    @Autowired
    private IFirmwareService firmwareService;

    @Autowired
    private FirmwareTaskPoMapper taskPoMapper;
    @Autowired
    private FirmwareTaskDetailPoMapper taskDetailPoMapper;
    @Autowired
    private IUpgradeService upgradeService;
    @Autowired
    private IDeviceService deviceService;

    @RequestMapping(value = "create", method = RequestMethod.POST)
    @PreAuthorize("@ps.hasRole('admin')")
    @ApiOperation(value = "新增任务", httpMethod = "POST", response = DataResult.class, notes = "新增任务")
    public DataResult add(@RequestBody  FirmwareTaskPo req) {
        FirmwareTaskPo po = new FirmwareTaskPo();
        po.setTaskName(req.getTaskName());
        int total = taskPoMapper.selectCount(po);
        if(total > 0){
            throw BCGException.genException(BCErrorEnum.FIRMWARE_TASK_UNIQUE_ERROR);
        }
        po.setFirmwareId(req.getFirmwareId());
        taskPoMapper.insertSelective(po);
        return DataResult.ok();
    }

    @RequestMapping(value = "search", method = RequestMethod.POST)
    @ApiOperation(value = "分页查询固件列表", httpMethod = "POST", response = DataResult.class, notes = "分页查询固件列表")
    public DataResult<PageResDto<FirmwareTaskPo>> search(@RequestBody PageReqDto<FirmwareTaskPo> searchPage) {
        Integer pageNo = searchPage.getPageNo();
        Integer limit = searchPage.getLimit();
        FirmwareTaskPo query = new FirmwareTaskPo();
        query.setDelFlag(0);
        query.setFirmwareId(searchPage.getParamData().getFirmwareId());
        PageHelper.startPage(pageNo, limit, "id desc");
        List<FirmwareTaskPo> list = taskPoMapper.select(query);
        Integer total = taskPoMapper.selectCount(query);
        return DataResult.ok(PageResDto.genResult(pageNo, limit, total, list, null));
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    @PreAuthorize("@ps.hasRole('admin')")
    @ApiOperation(value = "删除任务", httpMethod = "GET", response = DataResult.class, notes = "删除任务")
    public DataResult delete(Long id) {
        taskPoMapper.deleteById(id);
        return DataResult.ok();
    }

    @RequestMapping(value = "upgrade", method = RequestMethod.POST)
    @PreAuthorize("@ps.hasRole('admin')")
    @ApiOperation(value = "升级", httpMethod = "POST", response = DataResult.class, notes = "升级")
    public DataResult upgrade(@RequestBody UpgradeReqDto reqDto) {
        if(null  != reqDto.getUpgradeTime() ){
            if((new Date()).after(reqDto.getUpgradeTime())){
               throw new BCGException(BCErrorEnum.FAIL.getCode(), "时间不得早于当前时间", "");
            }
            DelayUpgradeProducer.send(reqDto);
            FirmwareTaskPo taskPo = new FirmwareTaskPo();
            taskPo.setId(reqDto.getTaskId());
            taskPo.setBookTime(reqDto.getUpgradeTime());
            taskPoMapper.updateByPrimaryKeySelective(taskPo);
        }else {
            upgradeService.sendUpgrade(reqDto);
        }

        return DataResult.ok();
    }

    @RequestMapping(value = "detail", method = RequestMethod.POST)
    @ApiOperation(value = "分页查询升级详情列表", httpMethod = "POST", response = DataResult.class, notes = "分页查询固件列表")
    public DataResult<PageResDto<FirmwareTaskDetailPo>> taskDetail(@RequestBody PageReqDto<FirmwareTaskDetailPo> searchPage) {
        return DataResult.ok(firmwareService.taskDetail(searchPage));
    }

    @RequestMapping(value = "device", method = RequestMethod.POST)
    @ApiOperation(value = "设备搜索", httpMethod = "POST", response = DataResult.class, notes = "设备搜索")
    public DataResult<List<DevicePageResDto>> deviceSearch(@RequestBody UpgradeDeviceQueryReqDto reqDto) {
        return DataResult.ok(firmwareService.deviceSearch(reqDto));
    }
}
