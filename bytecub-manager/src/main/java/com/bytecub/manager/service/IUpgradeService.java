package com.bytecub.manager.service;

import com.bytecub.common.domain.dto.request.upgrade.UpgradeReqDto;

/**
 * com.bytecub.manager.service
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/12
 */
public interface IUpgradeService {
    void sendUpgrade(UpgradeReqDto reqDto);
}
