package com.bytecub.manager.mq;

import com.bytecub.common.domain.dto.request.upgrade.UpgradeReqDto;

/**
 * com.bytecub.manager.mq
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/29
 */
public class DelayUpgradeProducer {

    public static void send(UpgradeReqDto reqDto){
        DelayUpgradeStorage.push(reqDto);
    }
}
