package com.bytecub.mdm.service;

import com.bytecub.common.domain.DataResult;
import com.bytecub.common.domain.dto.PageReqDto;
import com.bytecub.common.domain.dto.PageResDto;
import com.bytecub.common.domain.dto.request.firmware.FirmwareCreateReqDto;
import com.bytecub.common.domain.dto.request.firmware.FirmwareQueryReqDto;
import com.bytecub.common.domain.dto.request.upgrade.UpgradeDeviceQueryReqDto;
import com.bytecub.common.domain.dto.response.device.DevicePageResDto;
import com.bytecub.mdm.dao.po.FirmwarePo;
import com.bytecub.mdm.dao.po.FirmwareTaskDetailPo;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * com.bytecub.mdm.service
 * project bytecub
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/8
 */
public interface IFirmwareService {
    PageResDto<FirmwarePo> searchByPage(PageReqDto<FirmwareQueryReqDto> pageReqDto);
    void create(FirmwarePo po);
    void updateById(FirmwarePo po);
    void deleteById(Long id);
    FirmwarePo queryById(Long id);
    List<DevicePageResDto> deviceSearch(@RequestBody UpgradeDeviceQueryReqDto reqDto);
    PageResDto<FirmwareTaskDetailPo> taskDetail(@RequestBody PageReqDto<FirmwareTaskDetailPo> searchPage);
}
