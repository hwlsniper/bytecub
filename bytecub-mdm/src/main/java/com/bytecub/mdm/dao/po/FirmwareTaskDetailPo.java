package com.bytecub.mdm.dao.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Data
@Table(name = "t_firmware_task_detail")
public class FirmwareTaskDetailPo {
    @Id
    private Long id;

    private Long taskId;

    private String deviceCode;

    private Integer upgradeStatus;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern ="yyyy-MM-dd HH:mm:ss",timezone ="GMT+8" )
    private Date createTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern ="yyyy-MM-dd HH:mm:ss",timezone ="GMT+8" )
    private Date updateTime;

    private String detailMsg;

    private String messageId;

    @Transient
    private String taskName;
    @Transient
    private String firmwareName;
    @Transient
    private String firmwareVersion;


}