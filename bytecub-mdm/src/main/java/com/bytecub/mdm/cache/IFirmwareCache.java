package com.bytecub.mdm.cache;

/**
 * com.bytecub.mdm.cache
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/13
 */
public interface IFirmwareCache {
    String writer(Long firmwareId, String url);
    String reader(Long firmwareId);
    void remove(Long firmwareId);
}
