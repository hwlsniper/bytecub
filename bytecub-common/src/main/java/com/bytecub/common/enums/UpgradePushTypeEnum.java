package com.bytecub.common.enums;

import io.swagger.models.auth.In;
import lombok.Getter;

/**
 * com.bytecub.common.enums
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/14
 */
@Getter
public enum UpgradePushTypeEnum {
    URL(1, "URL升级"),
    FILE(2, "文件升级"),
    MIXED(3, "混合升级");
    Integer type;
    String msg;

    UpgradePushTypeEnum(Integer type, String msg) {
        this.type = type;
        this.msg = msg;
    }
}
