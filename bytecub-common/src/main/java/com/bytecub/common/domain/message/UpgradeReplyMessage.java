package com.bytecub.common.domain.message;

import lombok.Data;

/**
 * com.bytecub.common.domain.message
 * project bytecub  bytecub.cn
 * 升级指令返回
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/14
 */
@Data
public class UpgradeReplyMessage {
    String messageId;
    Integer code;
    String msg;
}
