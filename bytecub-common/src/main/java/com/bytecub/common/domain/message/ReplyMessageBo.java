package com.bytecub.common.domain.message;

import lombok.Data;

/**
 * com.bytecub.common.domain.message
 * project bytecub  bytecub.cn
 * 设备回执信息
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/18
 */
@Data
public class ReplyMessageBo {
    Integer code;
    String msg;
}
