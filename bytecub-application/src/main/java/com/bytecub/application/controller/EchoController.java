package com.bytecub.application.controller;

import com.bytecub.common.domain.DataResult;
import com.bytecub.common.domain.bo.ctwing.CtOnlineMessage;
import com.bytecub.common.domain.bo.ctwing.CtReportMessage;
import com.bytecub.mdm.service.IDeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
  * ByteCub.cn.
  * Copyright (c) 2020-2020 All Rights Reserved.
  * 
  * @author bytecub@163.com  songbin
  * @version Id: EchoController.java, v 0.1 2020-12-02  Exp $$
  */

@Slf4j
@RestController
@Api(value = "DB测试")
public class EchoController {
    @Autowired
    private IDeviceService deviceService;

    @RequestMapping(value = "/ctwing/test", method = RequestMethod.GET)
    @ApiOperation(value = "DB测试", httpMethod = "GET", response = DataResult.class, notes = "DB测试")
    public String echo( String data) {
        deviceService.queryByDevCode(data);
        return "ok";
    }
    @RequestMapping(value = "/ctwing/online", method = RequestMethod.POST)
    public String echo( @RequestBody CtOnlineMessage data) {
        log.warn("收到上下线数据:{}", data);
        return "ok";
    }
}
