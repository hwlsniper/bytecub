package com.bytecub.gateway.mq.services.impl;

import com.bytecub.gateway.mq.services.IDownMessageParseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created on 2021/8/26.
 *
 * @author songbin songbin.sky@hotmail.com
 */
@Slf4j
@Service
public class DownMessageParseServiceImpl implements IDownMessageParseService {
}
