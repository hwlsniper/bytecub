package com.bytecub.gateway.mq.domain;

import lombok.Data;

/**
 * Created on 2021/8/24.
 *
 * @author songbin songbin.sky@hotmail.com
 */
@Data
public class TopicMsgBo {
    private String topic;
    private byte[] msg;
}
