package com.bytecub.gateway.mq.excutor;

import com.bytecub.common.biz.TopicBiz;
import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.dto.response.device.DevicePageResDto;
import com.bytecub.common.domain.message.DeviceDownMessage;
import com.bytecub.common.enums.BCErrorEnum;
import com.bytecub.common.enums.TopicTypeEnum;
import com.bytecub.common.exception.BCGException;
import com.bytecub.common.domain.gateway.mq.PropertyReaderMessageBo;
import com.bytecub.gateway.mq.domain.TopicMsgBo;
import com.bytecub.gateway.mq.mqttclient.BcPubMqttClient;
import com.bytecub.gateway.mq.services.PluginHelperService;
import com.bytecub.mdm.dao.po.ProductPo;
import com.bytecub.mdm.service.IDeviceService;
import com.bytecub.mdm.service.IProductService;
import com.bytecub.protocol.base.IBaseProtocol;
import com.bytecub.protocol.service.IProtocolUtilService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *  * ByteCub.cn.  * Copyright (c) 2020-2020 All Rights Reserved.  *   * @author bytecub@163.com songbin
 *  * @version Id: MQTTConsumeMonitor.java, v 0.1 2020-12-09  Exp $$  
 */
@Service
@Slf4j
public class PropertyReaderExecutor {

    @Autowired
    BcPubMqttClient bcPubMqttClient;
    @Autowired
    IProductService productService;
    @Autowired
    IProtocolUtilService protocolUtilService;
    @Autowired
    private PluginHelperService pluginHelperService;
    @Autowired
    private IDeviceService deviceService;
    /***
     * 具体线程
     */
    @Async(BCConstants.TASK.PROPERTY_READER_NAME)
    public void execute(PropertyReaderMessageBo msg) {
        try {
            this.sendMsg(msg.getDevicePageResDto(), msg.getDeviceDownMessage());
        } catch (Exception e) {
            log.warn("内部队列消费异常", e);

        }

    }
    /**
     * 发送请求到设备端
     */
    private void sendMsg(DevicePageResDto devicePageResDto, DeviceDownMessage deviceDownMessage) {
        if(null == deviceService.queryByDevCode(devicePageResDto.getDeviceCode())){
            log.warn("设备不存在:{}", devicePageResDto);
            throw new BCGException("设备不存在");
        }
        TopicMsgBo topicMsgBo = pluginHelperService.encode(TopicTypeEnum.PROP_READ, devicePageResDto.getDeviceCode(), deviceDownMessage);
        bcPubMqttClient.publish(topicMsgBo.getTopic(), topicMsgBo.getMsg());
    }

}
